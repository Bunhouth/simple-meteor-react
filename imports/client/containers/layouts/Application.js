import React from 'react';
import { createContainer } from 'meteor/react-meteor-data';

import ApplicationLayout from '../../components/layouts/ApplicationLayout';

export default Application = createContainer(() => {
  const currentUser = Meteor.user();
  return {
    currentUser: currentUser,
    isSignedIn: currentUser && true,
  };
}, ApplicationLayout);