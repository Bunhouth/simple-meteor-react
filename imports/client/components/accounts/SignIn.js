import React from 'react';
import classnames from 'classnames';
import Input from '../form/Input';

export default class SignIn extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      form: {
        email: null,
        password: null
      },
      error: {}
    }

    this.onLogin = this.onLogin.bind(this);
    this.onInputChange = this.onInputChange.bind(this);
  }

  onLogin() {
    const { form } = this.state;
    let { error } = this.state;

    if(this.validate()) {
      Meteor.loginWithPassword(form.email, form.password, (response) => {
        if(response){
          error['message'] = response.reason;
          this.setState({ errors: error })
        } else {
          this.setState({ error: {}, form: {
            email: null,
            password: null
          } })
        }
      })
    }
  }

  validate() {
    const { form } = this.state;
    let error = this.state.error;

    const validData = Object.keys(form).map((key) => {
      if(_.isEmpty(form[key])) {
        error[key] = 'can\'\t be blank';
        return !_.isEmpty(form[key]);
      } else {
        delete error[key]
        return true;
      }
    });
    this.setState({ error: error })
    return !validData.includes(false)
  }

  onInputChange(field, value) {
    let { form } = this.state;
    form[field] = value;

    this.setState({ form: form });
  }

  render() {
    const { active } = this.props;
    const { error } = this.state;
    const cx = classnames('row col-md-22', {'hidden': !active})

    return (
      <div className={cx}>
        {
          error['message'] &&
            <p className="alert alert-danger">{ error['message'] }</p>
        }
        <form autoComplete="off">
          <Input placeholder="Username or email" name="email" isError={error['email'] && true} errorMessage={error['email']} onChange={(f, e) => this.onInputChange(f, e)} />
          <Input type="password" placeholder="Password" name="password" isError={error['password'] && true} errorMessage={error['password']} onChange={(f, e) => this.onInputChange(f, e)} />

          <button type="button" className="btn btn-danger" name="login" onClick={this.onLogin}>Login</button>
        </form>
      </div>
    );
  }
}
