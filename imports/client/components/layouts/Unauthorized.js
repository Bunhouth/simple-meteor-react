import React from 'react';
import Footer from '../shared/Footer';
import SignUp from '../accounts/SignUp';
import SignIn from '../accounts/SignIn';
import ActiveUsers from '../shared/ActiveUsers';

export default class Unauthorized extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      step: 'signIn'
    }
  }

  onChangeAction() {
    const { step } = this.state;
    const newStep = step == 'signIn' ? 'signUp' : 'signIn';

    this.setState({ step: newStep })
  }

  render() {
    const { step } = this.state;

    return (
      <div id="welcome-page" className="welcome-page animated fadeIn">
        <div className="row-welcome" style={{backgroundImage: "url('http://demos.bootdey.com/bookpost/img/Post/home-bg.jpg')" }}>
          <div className="row-body">
            <div className="welcome-inner">
              <div className="welcome-message welcome-text-shadow">
                <div className="welcome-title">
                  Welcome
                </div>

                <div className="welcome-desc">
                  to our social network
                </div>

                <div className="welcome-about">
                  share your memories, connect with others, make new friends.
                </div>
              </div>

              <div className="welcome-inputs">
                <div className="widget panel-inputs panel-home animated fadeInUp">
                  <div className="widget-header">
                    <h3 className="widget-caption">
                      <span className="pull-right">
                        <a onClick={this.onChangeAction.bind(this)} className="btn btn-danger btn-xs btn-panel-home">{ step == 'signIn' ? 'Register' : 'Login'}</a>
                      </span>
                    </h3>
                  </div>

                  <div className="widget-body">
                    <div className="row">
                      <SignUp active={step === 'signUp'} />
                      <SignIn active={step === 'signIn'} />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <ActiveUsers />
        <Footer />
      </div>
    );
  }
}
