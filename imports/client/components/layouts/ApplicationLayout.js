import React from 'react';

import Navbar from '../shared/Navbar';
import Footer from '../shared/Footer';
import Unauthorized from './Unauthorized';
import Authorized from './Authorized';
import LoadingScreen from '../shared/LoadingScreen';

export default class ApplicationLayout extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isReady: false
    }
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({ isReady: true })
    }, 100)
  }

  render() {
    const { isSignedIn } = this.props;
    const screen = isSignedIn ? <Authorized {...this.props} /> : <Unauthorized />
    return (
      <div>
        {
          this.state.isReady ?
            screen
            :
            <LoadingScreen />
        }
      </div>
    );
  }
}
