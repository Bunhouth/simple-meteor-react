import React from 'react';
import Navbar from '../shared/Navbar';

export default class Authorized extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div id="main-page" className="container">
        <Navbar currentUser={this.props.currentUser }/>

        <div style={{ marginTop: '65px' }}>
          { this.props.yield }
        </div>
      </div>
    );
  }
}
