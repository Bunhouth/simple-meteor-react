import React from 'react';
import classnames from 'classnames';

export default class Input extends React.Component {
  render() {
    const { type, placeholder, name, isError, id, className, errorMessage } = this.props;
    const cx = classnames('form-group', { 'has-error': isError });

    return (
      <div className={cx}>
        <input type={type} id={id} name={name} placeholder={placeholder} className={className} onChange={(e) => this.props.onChange(name, e.target.value)} />
        { isError && <span className="pull-left help-block">{ errorMessage }</span> }
      </div>
    );
  }
}


Input.defaultProps = {
  type: 'text',
  placeholder: 'Some input here ...',
  id: + new Date(),
  name: + new Date(),
  className: '',
  isError: false,
  errorMessage: ''
}