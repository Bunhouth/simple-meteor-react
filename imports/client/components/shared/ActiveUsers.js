import React from 'react';

const ActiveUsers = ({}) => {
  return (
    <div className="row-body">
      <div className="welcome-users-inner">
        <div className="welcome-user">
          <a href="http://demos.bootdey.com/bookpost/profile.html">
            <img src="./images/guy-3.jpg" className="img-circle" />
          </a>
        </div>

        <div className="welcome-user">
          <a href="http://demos.bootdey.com/bookpost/profile.html">
            <img src="./images/woman-1.jpg" className="img-circle" />
          </a>
        </div>

        <div className="welcome-user">
          <a href="http://demos.bootdey.com/bookpost/profile.html">
            <img src="./images/guy-2.jpg" className="img-circle" />
          </a>
        </div>

        <div className="welcome-user">
          <a href="http://demos.bootdey.com/bookpost/profile.html">
            <img src="./images/woman-2.jpg" className="img-circle" />
          </a>
        </div>

        <div className="welcome-user">
          <a href="http://demos.bootdey.com/bookpost/profile.html">
            <img src="./images/guy-5.jpg" className="img-circle" />
          </a>
        </div>

        <div className="welcome-user">
          <a href="http://demos.bootdey.com/bookpost/profile.html">
            <img src="./images/woman-3.jpg" className="img-circle" />
          </a>
        </div>

        <div className="welcome-user">
          <a href="http://demos.bootdey.com/bookpost/profile.html">
            <img src="./images/guy-8.jpg" className="img-circle" />
          </a>
        </div>

        <div className="welcome-user">
          <a href="http://demos.bootdey.com/bookpost/profile.html">
            <img src="./images/woman-4.jpg" className="img-circle" />
          </a>
        </div>

        <div className="welcome-user">
          <a href="http://demos.bootdey.com/bookpost/profile.html">
            <img src="./images/guy-9.jpg" className="img-circle" />
          </a>
        </div>

        <div className="welcome-user">
          <a href="http://demos.bootdey.com/bookpost/profile.html">
            <img src="./images/woman-7.jpg" className="img-circle" />
          </a>
        </div>
      </div>
    </div>
  );
}

export default ActiveUsers;
