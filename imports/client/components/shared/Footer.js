import React from 'react';

export default class Footer extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <footer className="welcome-footer">
        <div className="container">
          <p>
            </p><div className="footer-links">
              <a href="http://demos.bootdey.com/bookpost/#">Terms of Use</a> |
              <a href="http://demos.bootdey.com/bookpost/#">Privacy Policy</a> |
              <a href="http://demos.bootdey.com/bookpost/#">Developers</a> |
              <a href="http://demos.bootdey.com/bookpost/#">Contact</a> |
              <a href="http://demos.bootdey.com/bookpost/#">About</a>
            </div>
            Copyright © Company - All rights reserved
          <p></p>
        </div>
      </footer>
    );
  }
}
