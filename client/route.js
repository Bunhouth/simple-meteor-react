import React from 'react';
import Application from '../imports/client/containers/layouts/Application';

import Home from '../imports/client/components/Home';

FlowRouter.route("/", {
  action: () => {
    ReactLayout.render(Application, {
      yield: <Home />
    });
  }
});

FlowRouter.route('/logout', {
  action: function () {
    Meteor.logout(function() {
      FlowRouter.redirect('/')
    })
  }
});
